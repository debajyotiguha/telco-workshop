# Step 1: Setting Our Policies
 
1. As it stands we currently have a Secret Detection job included in our pipeline that will alert us if any secrets are detected in the code base when it runs. However that doesn't mean that it will block the current code from being merged. To do this we will need to create a secret detection policy to specifically block these merges.
  
2. To prevent this from ever happening in the future we can set up a new policy to run on all future merge requests. For our use case leaked tokens are easy mistakes that can lead to massive problems so we will create a quick policy to stop that. Use the left hand navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Merge Request Approval Policy_**.
  
3. Add a **name** to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Secret Detection_** and **All protected branches** to **default branch**. Change the _Severity is_ to be **All severity levels** and _Status is_ to be **New & All vulnerability states**
  
4. Then under actions choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver.

5. Lastly click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code base merging would be prevented until it was removed or you added your approval.
  
6. Lastly use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Docs for policies](https://docs.gitlab.com/ee/user/application_security/policies/)

# Step 2: Run Our Pipeline
 
1. Now that we have fully set up our pipeline lets go ahead and check in on the merge request. Use the left hand navigation menu to click through **Code>Merge request** then click into the merge request we created earlier.

2. Notice that our policies are being applied. Because no secret was found in our code base we are able to merge our code. We can click **Merge** which will kick off our pipeline running on main.

3. Now that our pipeline is merged we can use the left hand navigation menu to click through **Plan> Issue Board** and move the CICD issue into closed.

> We will go to a quick break while the pipeline runs

4.   Now that our pipeline has completed lets check in. Use the left hand navigation menu to click through **Build>Pipelines** and click into the most recently kicked off pipeline. We should be able to see that our .pre and .post jobs have run.

# Step 3: Parsing the Results

1. Now that your **_main_** pipeline has completed the reports under **_Security & Compliance_** have been generated. These reports will only be generated if you run a pipeline against main.
  
2. Use the left hand navigation menu to click through **Secure-\> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes a long time to collect data so if yours still has no results your presenter will show you the dashboard of a deployed application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard)
  
3. We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report
  
4. First change the _All tools_ section under **Tools** to just filter on SAST. We can then click into any of the SAST vulnerabilities shown.
  
5. Inside the vulnerability we will want to click the _Explain vulnerability_ button within the **Explain this vulnerability and how to mitigate it with AI** section. This will result in a popup appearing on the right hand side with some information on what the vulnerability is and how you can fix it. The Explain This Vulnerability feature currently works on any SAST vulnerabilities.

6. Lastly we want to close out the issue related to this work on our issue board. Use the left hand navigation menu to click through **Plan>Issue boards** and move the security issue to closed.
