# Step 1: Prep For Our Release

1. Before we start to create our release we want to grab the image we created in our build. Lets go ahead and use the left hand navigation menu to click through **Deploy>Container Registry**

2. Once here we should only have one hosted image with the _main_ tag. Lets go ahead and copy that link locally to use for later.

3. As we run more pipelines our main image will always be updated, and in the future our tagged releases will be stored here as well.

# Step 2: Create Our Release

1. To start creating our first release we are going to use the left hand navigation menu to click through **Deploy>Releases**

2. Once on the release page we will click **Create a new release**. 

3. In the _Tag name_ input we will want to click the drop down then provide our own tag **_1.0.0_**, then click **Create tag 1.0.0**. Add a quick message then click **Save**

4. Next change the _Release title_ to be "Initial 1.0.0 Release".

5. Click the milestone dropdown and select _First Release Milestone_.

6. Leave the release date as today, add a quick release note, then check _Include message from the annotated tag_

7. We then are going to attach the release image we fetched earlier. In the URL field add our image URL, set the title to Release Image, then change the option from _Other_ to _Image_

7. Leave the rest of the settings as is then click **Create release**. 

8. We now have a released version of our current application! Next we will start to plan our upcoming release.

# Step 3: Plan Another release

1. Navigate back to the main release page by using the left hand navigation menu to click through **Deploy>Releases**

2. In the _Tag name_ input we will want to click the drop down then provide our own tag **_1.0.1_**, then click **Create tag 1.0.1**. Add a quick message stating this is for a future release then click **Save**

3. Next change the _Release title_ to be "Future 1.0.1 Release".

4. Set the release date as the end date of our last release, add a quick release note, then check _Include message from the annotated tag_

5. We also want to pair this release with a couple of scheduled events we have coming up so we are going to add some release assets. In the URL field add this URL: https://about.gitlab.com/events/

6. Set the _title_ as release events, then leave the rest of the settings as is then click **Create release**. 

7. Now in our release view we can see that we have a scheduled release! 

> Other ways to schedule/set up releases: https://docs.gitlab.com/ee/user/project/releases/#create-a-release-in-the-releases-page

8. Lastly we will want to use the left hand navigation menu to click through **Plan>Issue boards** and move any remaining open issues to closed.
