# Step 01 - Basic Pipeline

1. Now that we have our merge request all set up we are going to start working on our CICD pipeline. We could use the built in Web IDE to write our ci file, but in this case we are going to use the _Pipeline Editor_. Use the left hand navigation menu to click through **Build>Pipeline editor**.

2. We are going to start by clicking **Configure pipeline**. This will add in some placeholder CICD code that we want to fully delete. Next we will want to change the branch in the top left from **main to the branch our merge request created**

3. Next add the base of our pipeline as seen below:

```
image: docker:latest

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  

stages:
  - build
 
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker build -t $IMAGE .
```

4. This is a pretty basic pipeline that will build our application into a docker image. What if we wanted to add a simple test however? We can do that by first adding a new test stage:

```
stages:
  - build
  - test
```

5. Then actually add the test job to the end of the config:

```
test:
  stage: test
  image: gliderlabs/herokuish:latest
  script:
    - cp -R . /tmp/app
    - /bin/herokuish buildpack test
```

6. Now we have successfully set up a basic pipeline that will build and run a small test on our application! Before committing and running this code we will want to complete the next two sections to fully round out our pipeline.

# Step 02 - Advanced Pipeline

1. In this section we are going to add some security scanners, rules, and speed up the pipeline with caching.

2. First we will add the scanners which will take advantage of the existing _test_ stage we have already defined. To do this we will add the templates below to our project:

```
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml
```

3. By clicking the _Full configuration_ tab we can see that we brought in a ton of new code to our pipeline that will help us. We can also click the tree icon in the top left to get a list of the different yaml files we have brought into our pipeline. Each of these new templates will enable us to run our security scans.

4. In this case we dont want some of our scans running every single time we make a commit. For our use case we only want the _Code Quality_ scan to run when we are running against main, and our _SAST-IAC_ job to only run if we modify the Docker file.

5. The code below will override the currently template configs to fit our needs:

```
code_quality:
  rules:
    - if: '$CI_COMMIT_REF_NAME == "main"'

iac-sast:
  rules:
    - changes:
      - DOCKERFILE
```

6. Lastly we also want to speed up our whole pipeline using caching. We can just store the generated ruby files instead of re-creating them each run with the code below:

```
cache:
  - key: cache-$CI_COMMIT_REF_SLUG
    fallback_keys:
      - cache-$CI_DEFAULT_BRANCH
      - cache-default
    paths:
      - vendor/ruby
```

7. Now we know our code is secure, the scans run when they should, and our overall pipeline is running faster.


# Step 03 - Prepping The Release Job

1. In this last section we will actually push out our build image to the internal package registry so that we can use it in our releases. To do so we will just need to modify the build job as seen below:

```
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
  after_script:
    - docker push $IMAGE
```

2. Now instead of just building the image we also log into the built in registry and push our image out. Next we will double check in the top left that we are on our merge request branch, then scroll to the bottom of the page and click **Commit changes**.

3. Lets then use the left hand navigation menu to click through **Code>Merge requests** where we can then see our new pipeline has kicked off.

> If you get stuck at any point please reference the ci-reference.yml file
