# Step 01 - Create Project Labels

1. Use the left hand navigation menu to click through **Manage** > **Labels**
  
2. Click **New label**
  
3. Enter **P1** in the ***Title*** field and select a color
  
4. Click **Create label**
  
5. Click **New label** and create another label for **P2**
  
6. Click **New label** and create another label for **Workflow::Planning**
  
7. Click **New label** and create another label for **Workflow::WorkInProgress**

8. Click **New label** and create another label for **Workflow::Ready**


# Step 02 - Create New Board

1. Use the left hand navigation menu to click through **Plan -> Issue boards**
  
2. Click **Create new board** within the dropdown that says **Development**.

3. Enter a title (**Team-Scoped-Board**)
  
4. Leave **Show the Open list** and **Show the Closed list** checkboxes selected
  
5. Click **Create board**

> You may need to swap to your new board if it dosent change automatically
  
6. Click **Create list**. Leave the scope of the list set to **Label**. Select **Workflow::Planning**. Click **Add to board**.

7. Click **Create list**. Leave the scope of the list set to **Label**. Select **Workflow::WorkInProgress**. Click **Add to board**.

7. Click **Create list**. Leave the scope of the list set to **Label**. Select **Workflow::Review**. Click **Add to board**.
  
8. As we add issues in the future if they contain the scoped labels they will automatically appear in the labels board.


# Step 03 - Creating Our Milestones

1. First we will want to create our milestones as they will relate to our releases later in the workshop and help group our issues. Use the left hand navigation menu to click through **Plan>Milestones**.

2. Next we will click **New milestone**. Set the _Title_ of this milestone to be **_First Release Milestone_**, Set the _Start Date_ as today and _Due Date_ a week out from today.

3. Lastly add a quick description then click **Create milestone**. Repeat this process again for your _Second Release Milestone_, this time setting the _Start Date_ a week out from now and the _Due Date_ a week after then.


# Step 04 - Creating Our Issues

1. Navigate to **Plan>Issues** using the left hand navigation menu, then click **New issue**

2. On the resulting issue creation page title our issue **_Planning Our Work_**

3. Then in the description area provide the description _"Create 3 separate issues for setting up CICD, securing our application, and planning a release"_

4. Assign the issue to yourself (Even if your name doesn't appear in the dropdown you can still write in your GitLab id), give the issue a weight, select our first milestone, then select a due date 1 week out.

5. Once done we want to then click **Create Issue**.

6. We still want to create 3 other issues to track the rest of our planned work. Use the breadcrumbs at the top of the page to click back to the _Issues_ view, then once again we will start by clicking **New Issue**

7. We will title this issue **Setup GitLab CICD**, then in the description area ensure that we are using _plain text editing_

8. Next click the **tanuki icon** in the description field, then click  **Generate issue description** in the resulting dropdown and use this prompt: "For this issue we want to enable GitLab CICD for our project", then click **Submit**

9. Set the milestone to be our first milestone, fill in the rest of the details as you see fit, then click **Create issue**

10. Assign Label **Workflow::Planning**

11. Repeat this process following your instructors guidances to create issues for _Securing Our Application_ and _Planning Our Release_


# Step 05 - Job Boards

1. Navigate to **Plan**>**Issue Boards**
  
2. If not already selected, select the **Team-Scoped-Board** board from the board selector dropdown
  
3. Note the issues in the **Open** list & the other issues in the _Workflow::Planning_ list
  
4. Click and drag the **Create 3 separate issues for setting up CICD, securing our application, and planning a release** issue into the **Workflow::Planning** list
  
5. Click on that issue and note the labels now include **Workflow::Planning**
  
6. Click and drag the same issue into **Workflow::WorkInProgress**
  
7. Note the issue labels now include **Workflow::WorkInProgress** and *not* **Workflow::Planning**
  
8. Click and drag the same issue into Closed. This closes the issue.
    
9. Before moving on, re-open the issue by dragging the closed issue back to ***Workflow::Planning***

> Work In Progress limits can be set on any board list (column) - using the "edit list settings" icon


# Step 06 - Create Merge Request

Before we can secure our application or plan a release we need to set up our CICD pipeline. 

1. Let's start by moving the issue related to setting up CICD to ***Workflow::WorkInProgress*** column

2. We are now ready to work on the issue, so lets go ahead by clicking into the issue related to setting up CICD. 

3. You might have to scroll but we then want to click **Create merge request**.

4. On the resulting new merge request page we will first want to uncheck **Mark as draft**

5. We can then assign the it to ourselves, then scroll down and click **Create merge request**

6. On the resulting merge request we can then see that our issue is already linked and all of our further work will be tracked here


# Step 07 - Asking Duo Chat

1. Now that we have covered a small subset of GitLab's plan features you may have more questions. GitLab's newest AI feature GitLab Duo Chat can help answer that and more

2. In the top right corner of your screen go ahead and click the **GitLab Duo Chat** icon, to open the chat popup.

3. Let's start by asking chat "How can I create an Epic in GitLab?". You can also ask chat any GitLab related or coding questions. Feel free to use chat as an assistance tool for the rest of the workshop.

